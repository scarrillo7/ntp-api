const sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);

class MailController {
  async send({ params, request }) {
    const { user, event } = request.all();
    
    const html = params.type === 'general' ? this.generalHtmlBody(user, event) : this.pressHtmlBody(user, event);
    const msg = {
      to: user.email,
      from: 'info@productionsntp.com',
      subject: 'Registro',
      // text: 'and easy to do anywhere, even with Node.js',
      html,
    };
    sgMail.send(msg).catch((err) => {
      console.log('Error sendgrid: ', err);
    });
  }

  generalHtmlBody(user, event) {
    return `<table align="center" style="width: 600px; font-family: Trebuchet MS, Helvetica, sans-serif;" cellspacing="0">
		<tbody>
			<tr>
				<td
					style="background: url('https://firebasestorage.googleapis.com/v0/b/ntp-record.appspot.com/o/assets%2Fimages%2Fheader-correo.png?alt=media&amp;token=47f5e311-df80-4bb2-b9ee-1dcbcd780c61') center center; background-size: cover; height: 130px;">
				</td>
			</tr>

			<tr>
				<td style="border-top: 1px solid #444444; border-bottom: 1px solid #444444;">
					<table style="width: 100%;" cellspacing="0">
						<tbody>
							<tr>
								<td
									style="padding: 10px 0; width: 20%; border-left: 2px solid gray; border-right: 2px solid gray; text-align: center">
									<img style="width: 60px;"
										src="https://firebasestorage.googleapis.com/v0/b/ntp-record.appspot.com/o/assets%2Fimages%2Fntp-logo.png?alt=media&amp;token=fbfee5b7-87c4-4b61-beca-05ad62acb894" />
								</td>
								<td
									style="padding: 10px 0; width: 20%; text-align: center; border-right: 2px solid gray; font-size: 12px">
									<div style="text-align: center;">${event.date}</div>
								</td>
								<td
									style="padding: 10px 0; width: 20%; text-align: center; border-right: 2px solid gray; font-size: 12px">
									${event.address}</td>
								<td
									style="padding: 10px 0; width: 40%; text-align: center; font-size: 20px;  border-right: 2px solid gray">
									${event.name}</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>

			<tr>
				<td>
					<table style="width: 100%;" cellspacing="0">
						<tbody>
							<tr>
								<td style="padding: 5px 10px; width: 60%; font-size: 18px;">
									<div style="display: flex; align-items: center;">
										<div><img style="width: 100px;"
												src="https://firebasestorage.googleapis.com/v0/b/ntp-record.appspot.com/o/assets%2Fimages%2Fcontacto.png?alt=media&token=0ad55295-f67e-48e9-ba21-5e2594bdd485" />
										</div>
										<div style="text-align: center; font-size: 22px">
											<div>${user.firstName} ${user.lastName}</div>
										</div>
									</div>
								</td>
								<td style="padding-top: 5px;">
									<div style="text-align: center;">
										<div style="display: grid; grid-template-columns: 50% 50%; grid-gap: 0px; border: 1px solid black;">
											<div style="font-size: 12px; padding: 10px 0">&iexcl;Recuerda, tu <br> E-Ticket es
												&uacute;nico <br>
												e intransferible
											</div>
											<div style="margin-bottom: 10px; padding: 10px 0"><strong>N&deg; ${user.code}</strong></div>
										</div>
									</div>
									<div style="text-align: center; margin-bottom: 20px; margin-top: 5px; display: grid; grid-template-columns: 100%; grid-gap: 0px;">
										<img class="center" style="width: 35%; display: block; margin-left: auto; margin-right: auto; width: 50%;"
											src="https://firebasestorage.googleapis.com/v0/b/ntp-record.appspot.com/o/assets%2Fimages%2Fproductora.png?alt=media&amp;token=27870a1c-1492-4895-8c17-a57b63dee859" />
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>

			<tr>
				<td>
					<table style="width: 100%;" cellspacing="0">
						<tbody>
							<tr>
								<td style="text-align: center; width: 60%;"><img style="width: 90%;"
										src="https://firebasestorage.googleapis.com/v0/b/ntp-record.appspot.com/o/assets%2Fimages%2Fevent.png?alt=media&token=74b5b926-4e3c-4eeb-8b48-b6cf86c65339" />
								</td>
							</tr>
							<tr style="padding-top: 5px; padding-bottom: 5px;">
								<td style="width: 40%;">
									<div
										style="font-size: 12px; background: #e3e3e3; padding: 5px 5px; margin-bottom: 10px; text-align: center">
										<div>PARA CUALQUIER PREGUNTA, PONTE EN CONTACTO CON:&nbsp;productionsnewtime@gmail.com</div>
									</div>
									<div style="font-size: 10px; text-align: center;">CONDICIONES GENERALES DE VENTA</div>
									<div style="font-size: 8px; text-align: justify; padding-top: 5px">Para que sea
										v&aacute;lido, el e-ticket (entrada electr&oacute;nica) est&aacute; sujeto a las condiciones
										generales de venta de NTP, y en su caso tambi&eacute;n a las del Organizador que has aceptado a la
										hora de realizar tu pedido. RECORDATORIO: Este e-ticket no es reembolsable. Salvo que el Organizador
										prev&eacute; lo contrario, el e-ticket es personal, intransferible y no intercambiable. CONTROL DE
										ACCESO: El acceso al evento est&aacute; sujeto al control de validez de tu e-ticket. S&oacute;lo es
										v&aacute;lido este e-ticket para el lugar, la sesi&oacute;n, la fecha y hora del evento. Pasada la
										hora de inicio del evento, el acceso al evento ya no est&aacute; garantizado, y no te da derecho a
										ning&uacute;n reembolso. Por este motivo, te aconsejamos llegar con la suficiente antelaci&oacute;n
										respecto a la hora de inicio del evento. Para que sea v&aacute;lido este e-ticket debe ser impreso
										en papel A4 blanco en ambos lados, sin modificaci&oacute;n alguna del tama&ntilde;o de
										impresi&oacute;n y en buena calidad. Los e-tickets parcialmente impresos, sucios, da&ntilde;ados o
										ilegibles, no ser&aacute;n v&aacute;lidos y podr&aacute;n ser rechazados por el Organizador. El
										Organizador tambi&eacute;n se reserva el derecho de aceptar o rechazar otros soportes, incluso los
										electr&oacute;nicos (m&oacute;vil, Tablet, etc.). Cada e-ticket tiene un c&oacute;digo de barras que
										permite el acceso al evento a una &uacute;nica persona. Para que sea v&aacute;lido el e-ticket, no
										debe haber sido objeto de un impago, ya que en ese caso el c&oacute;digo de barras se
										desactivar&aacute;. Durante el control de acceso, deber&aacute;s presentar un RUT con foto de
										identidad en vigor. Despu&eacute;s del control de acceso, tendr&aacute;s que conservar este e-ticket
										hasta que finalice el evento. En algunos casos, el Organizador te entregar&aacute; una entrada de
										tipo talonario (en el que aparecer&aacute; o no los gastos de gesti&oacute;n). FRAUDE: Est&aacute;
										terminantemente prohibido reproducir, copiar, duplicar, falsificar este e-ticket de cualquiera
										manera que sea, y en caso de infracci&oacute;n, se ejercer&aacute;n las oportunas acciones legales.
										Del mismo modo, cualquier pedido realizado a trav&eacute;s de un medio de pago il&iacute;cito
										ser&aacute; objeto de una demanda judicial, y se invalidar&aacute; el e-ticket resultante de esta
										compra. RESPONSABILIDAD: El comprador es el &uacute;nico responsable del uso que se hace de sus
										e-tickets, por lo que en caso de p&eacute;rdida, robo o duplicado de un e-ticket v&aacute;lido,
										s&oacute;lo la primera persona que presentar&aacute; el e-ticket podr&aacute; acceder al evento. NTP
										se exime de cualquier responsabilidad respecto a las anomal&iacute;as que pudieran surgir durante el
										pedido, la gesti&oacute;n o impresi&oacute;n del e-ticket en la medida en que no las ha causado
										intencionalmente o cuando se producen por negligencia del comprador en caso de p&eacute;rdida, robo
										o uso il&iacute;cito del e-ticket. EVENTO: Los eventos son de la exclusiva responsabilidad del
										Organizador. La adquisici&oacute;n de este e-ticket supone obligatoriamente el cumplimiento del
										reglamento interno del lugar del evento y/o del Organizador. En caso de cancelaci&oacute;n o
										aplazamiento del evento, el reembolso del e-ticket (excluidos los gastos de transporte, alojamiento,
										etc.) estar&aacute; sujeto a las condiciones del propio Organizador (podr&aacute;s encontrar su
										correo electr&oacute;nico en la secci&oacute;n Informaci&oacute;n adicional) quien percibe los
										ingresos de la venta de los e-tickets.</div>
								</td>
							</tr>

							<tr>
								<td>
									<div style="padding: 10px; border: 1px solid #e3e3e3;">
										<div style="text-align: center;">
											<div>
												<img style="width: 50px; height: 40px;"
													src="https://firebasestorage.googleapis.com/v0/b/ntp-record.appspot.com/o/assets%2Fimages%2Fntp-logo.png?alt=media&amp;token=fbfee5b7-87c4-4b61-beca-05ad62acb894" />
											</div>
											<div style="font-weight: bold">Lider en Eventos</div>
										</div>
										<div style="text-align: center; font-size: 12px; padding: 5px 0;">&iquest;Eres organizador de
											eventos?</div>
										<div style="text-align: center; font-size: 10px;">Con <a href="www.ntp.cl">ntp.cl</a> crea tu propio
											canal de venta de entradas online y vende en tan solo 5 minutos.</div>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div>
										<div style="text-align: center; font-size: 10px;">Copyright &copy; 2019 NTP / Todos los derechos
											reservados.</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>`;
  }

  pressHtmlBody(user, event) {
    return `<table align="center" style="width: 600px; font-family: Trebuchet MS, Helvetica, sans-serif;" cellspacing="0">
		<tbody>
			<tr>
				<td
					style="background: url('https://firebasestorage.googleapis.com/v0/b/ntp-record.appspot.com/o/assets%2Fimages%2Fheader-correo.png?alt=media&amp;token=47f5e311-df80-4bb2-b9ee-1dcbcd780c61') center center; background-size: cover; height: 130px;">
				</td>
			</tr>

			<tr>
				<td style="border-top: 1px solid #444444; border-bottom: 1px solid #444444;">
					<table style="width: 100%;" cellspacing="0">
						<tbody>
							<tr>
								<td
									style="padding: 10px 0; width: 20%; border-left: 2px solid gray; border-right: 2px solid gray; text-align: center">
									<img style="width: 60px;"
										src="https://firebasestorage.googleapis.com/v0/b/ntp-record.appspot.com/o/assets%2Fimages%2Fntp-logo.png?alt=media&amp;token=fbfee5b7-87c4-4b61-beca-05ad62acb894" />
								</td>
								<td
									style="padding: 10px 0; width: 20%; text-align: center; border-right: 2px solid gray; font-size: 12px">
									${event.date}</td>
								<td
									style="padding: 10px 0; width: 20%; text-align: center; border-right: 2px solid gray; font-size: 12px">
									${event.address}</td>
								<td
									style="padding: 10px 0; width: 40%; text-align: center; font-size: 20px;  border-right: 2px solid gray">
									${event.name}</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>

			<tr>
				<td>
					<table style="width: 100%;" cellspacing="0">
						<tbody>
							<tr>
								<td style="padding: 5px 10px; width: 60%; font-size: 18px;">
									<div style="display: flex; align-items: center;">
										<div><img style="width: 100px;"
												src="https://firebasestorage.googleapis.com/v0/b/ntp-record.appspot.com/o/assets%2Fimages%2Fidentification-card.png?alt=media&amp;token=b1a8fb25-4c5f-49ef-9fb9-543ac8b6d155" />
										</div>
										<div style="text-align: center; padding-left: 20px;">
											<div>${user.firstName} ${user.lastName}</div>
											<div>${user.company}</div>
										</div>
									</div>
								</td>
								<td style="padding-top: 5px;">
									<div style="text-align: center;">
										<div style="display: grid; grid-template-columns: 50% 50%; grid-gap: 0px; border: 1px solid black;">
											<div style="font-size: 12px; padding: 10px 0">&iexcl;Recuerda, tu <br> E-Ticket es &uacute;nico
												<br> e
												intransferible</div>
											<div style="margin-bottom: 10px; padding: 10px 0"><strong>N&deg; ${user.code}</strong></div>
										</div>
									</div>
									<div style="text-align: center; margin-bottom: 20px; margin-top: 5px; display: grid; grid-template-columns: 100%; grid-gap: 0px;">
										<img class="center" style="width: 35%; display: block; margin-left: auto; margin-right: auto; width: 50%;"
											src="https://firebasestorage.googleapis.com/v0/b/ntp-record.appspot.com/o/assets%2Fimages%2Fproductora.png?alt=media&amp;token=27870a1c-1492-4895-8c17-a57b63dee859" />
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>

			<tr>
				<td>
					<table style="width: 100%;" cellspacing="0">
						<tbody>
							<tr>
								<td style="text-align: center; width: 60%;"><img style="width: 90%;"
										src="https://firebasestorage.googleapis.com/v0/b/ntp-record.appspot.com/o/assets%2Fimages%2Fevent.png?alt=media&token=74b5b926-4e3c-4eeb-8b48-b6cf86c65339" />
								</td>
							</tr>

							<tr style="padding-top: 5px; padding-bottom: 5px;">
								<td style="width: 40%;">
									<div
										style="font-size: 12px; background: #e3e3e3; padding: 5px 5px; margin-bottom: 10px; text-align: center">
										<div>PARA CUALQUIER PREGUNTA, PONTE EN CONTACTO CON:&nbsp;productionsnewtime@gmail.com</div>
									</div>
									<div style="font-size: 10px; text-align: center;">CONDICIONES GENERALES DE VENTA</div>
									<div style="font-size: 8px; text-align: justify; padding-top: 5px">Para que sea
										v&aacute;lido, el e-ticket (entrada electr&oacute;nica) est&aacute; sujeto a las condiciones
										generales de venta de NTP, y en su caso tambi&eacute;n a las del Organizador que has aceptado a la
										hora de realizar tu pedido. RECORDATORIO: Este e-ticket no es reembolsable. Salvo que el Organizador
										prev&eacute; lo contrario, el e-ticket es personal, intransferible y no intercambiable. CONTROL DE
										ACCESO: El acceso al evento est&aacute; sujeto al control de validez de tu e-ticket. S&oacute;lo es
										v&aacute;lido este e-ticket para el lugar, la sesi&oacute;n, la fecha y hora del evento. Pasada la
										hora de inicio del evento, el acceso al evento ya no est&aacute; garantizado, y no te da derecho a
										ning&uacute;n reembolso. Por este motivo, te aconsejamos llegar con la suficiente antelaci&oacute;n
										respecto a la hora de inicio del evento. Para que sea v&aacute;lido este e-ticket debe ser impreso
										en papel A4 blanco en ambos lados, sin modificaci&oacute;n alguna del tama&ntilde;o de
										impresi&oacute;n y en buena calidad. Los e-tickets parcialmente impresos, sucios, da&ntilde;ados o
										ilegibles, no ser&aacute;n v&aacute;lidos y podr&aacute;n ser rechazados por el Organizador. El
										Organizador tambi&eacute;n se reserva el derecho de aceptar o rechazar otros soportes, incluso los
										electr&oacute;nicos (m&oacute;vil, Tablet, etc.). Cada e-ticket tiene un c&oacute;digo de barras que
										permite el acceso al evento a una &uacute;nica persona. Para que sea v&aacute;lido el e-ticket, no
										debe haber sido objeto de un impago, ya que en ese caso el c&oacute;digo de barras se
										desactivar&aacute;. Durante el control de acceso, deber&aacute;s presentar un RUT con foto de
										identidad en vigor. Despu&eacute;s del control de acceso, tendr&aacute;s que conservar este e-ticket
										hasta que finalice el evento. En algunos casos, el Organizador te entregar&aacute; una entrada de
										tipo talonario (en el que aparecer&aacute; o no los gastos de gesti&oacute;n). FRAUDE: Est&aacute;
										terminantemente prohibido reproducir, copiar, duplicar, falsificar este e-ticket de cualquiera
										manera que sea, y en caso de infracci&oacute;n, se ejercer&aacute;n las oportunas acciones legales.
										Del mismo modo, cualquier pedido realizado a trav&eacute;s de un medio de pago il&iacute;cito
										ser&aacute; objeto de una demanda judicial, y se invalidar&aacute; el e-ticket resultante de esta
										compra. RESPONSABILIDAD: El comprador es el &uacute;nico responsable del uso que se hace de sus
										e-tickets, por lo que en caso de p&eacute;rdida, robo o duplicado de un e-ticket v&aacute;lido,
										s&oacute;lo la primera persona que presentar&aacute; el e-ticket podr&aacute; acceder al evento. NTP
										se exime de cualquier responsabilidad respecto a las anomal&iacute;as que pudieran surgir durante el
										pedido, la gesti&oacute;n o impresi&oacute;n del e-ticket en la medida en que no las ha causado
										intencionalmente o cuando se producen por negligencia del comprador en caso de p&eacute;rdida, robo
										o uso il&iacute;cito del e-ticket. EVENTO: Los eventos son de la exclusiva responsabilidad del
										Organizador. La adquisici&oacute;n de este e-ticket supone obligatoriamente el cumplimiento del
										reglamento interno del lugar del evento y/o del Organizador. En caso de cancelaci&oacute;n o
										aplazamiento del evento, el reembolso del e-ticket (excluidos los gastos de transporte, alojamiento,
										etc.) estar&aacute; sujeto a las condiciones del propio Organizador (podr&aacute;s encontrar su
										correo electr&oacute;nico en la secci&oacute;n Informaci&oacute;n adicional) quien percibe los
										ingresos de la venta de los e-tickets.</div>
								</td>
							</tr>

							<tr>
								<td>
									<div style="padding: 10px; border: 1px solid #e3e3e3;">
										<div style="text-align: center;">
											<div>
												<img style="width: 50px; height: 40px;"
													src="https://firebasestorage.googleapis.com/v0/b/ntp-record.appspot.com/o/assets%2Fimages%2Fntp-logo.png?alt=media&amp;token=fbfee5b7-87c4-4b61-beca-05ad62acb894" />
											</div>
											<div style="font-weight: bold">Lider en Eventos</div>
										</div>
										<div style="text-align: center; font-size: 12px; padding: 5px 0;">&iquest;Eres organizador de
											eventos?</div>
										<div style="text-align: center; font-size: 10px;">Con <a href="www.ntp.cl">ntp.cl</a> crea tu propio
											canal de venta de entradas online y vende en tan solo 5 minutos.</div>
									</div>
								</td>
							</tr>
							<tr>
								<td>
									<div>
										<div style="text-align: center; font-size: 10px;">Copyright &copy; 2019 NTP / Todos los derechos
											reservados.</div>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</td>
			</tr>
		</tbody>
	</table>`;
  }
}

module.exports = MailController;
